#pyDNSpod
pyDNSpod项目是一个针对[DNSpod.cn](http://www.dnspod.cn)免费域名解析服务所提供的动态解析更新ip地址的脚本，开发语言为python 2.7(3.3)。
##软件原理
不停地检验通过路由器拨号获取的动态ip地址，如果与dnspod中配置的域名A记录地址不符合，即更新域名的A记录，以达到ddns的功能
主要针对adsl用户
##使用说明
共计3个文件
dnspod.py 为主程序，读取configure.txt的配置
configure.txt 存储dnspod账号和要修改的记录信息
setup.py 按提示输入信息后将查询结果存储入configure.txt以其供读取

只需要dnspod.py和configure.txt 2个文件即可工作，setup.py只在配置的时候有用，可以手动配置configure.txt，查询账号记录可访问 http://geekpi.cn/dnspod/ （来自官方的开源的php查询客户端）
配置完configure.txt之后即可

```sh
python dnspod.py
```
开始运行此程序

###linux 环境下
为了让dnspod.py随启动自动运行，可以将dnspod.py和configure.txt放入某个root账号能访问的目录如/opt/
配置/etc/rc.local文件，加入
```sh
/usr/bin/python /opt/dnspod.py &
```
让他随系统启动时后台运行
```sh
ps -AF|grep python
```
可以随时查看该程序运行状态

setup.py配置向导使用方法：
```sh
python setup.py
```
运行后提示
```sh
input dnspod.cn login email : 输入登陆账号，邮件地址
input dnspod.cn login password : 输入密码
```

接下来列出你账号下可管理的域名，从0..n编号在记录的最前面
```sh
which domain to set to ddns 
[0..n] in front of upon lines : 输入记录前面的编号
```

接下来列出这个域名下所有记录的详细信息，同样从0..n编号在记录的最前面
```sh
which record will be set to dynamic IP address : 输入记录前面的编号
```

最后显示确认信息和完成configure.txt的配置更新 （没有加入错误输入来带的问题，出错后重新运行一次就行了，配置选错了也可以再次运行，多次运行没有影响）
测试环境windows 7 chs 64bit python 2.7.3

###使用注意：
必须拥有外网可访问IP的环境，比如ADSL拨号获得的IP。由于部分ISP提供商会使用NAT设备分配虚拟IP，这种情况下程序获得的IP无法访问到你的设备，无法实现DDNS功能。另外部分ISP会封锁80端口的访问，这时可以选择其他没有被封的端口对外提供服务。


##License
Copyright(c) 2012-2013 geekpi. This software is licensed under the BSD license.