#!/usr/bin/env python
#-*- coding:utf-8 -*-

import httplib, urllib
import socket
import time
import ConfigParser

geekpi = ConfigParser.ConfigParser()

geekpi.read("configure.txt")

ddns_queue = geekpi.sections()

params = dict(
    login_email= geekpi.get(ddns_queue[0],"login_email"),
    login_password= geekpi.get(ddns_queue[0],"login_password"),
    format="json",
    domain_id=geekpi.get(ddns_queue[0],"domain_id"),
    record_id=geekpi.get(ddns_queue[0],"record_id"),
    sub_domain=geekpi.get(ddns_queue[0],"sub_domain"),
    record_line=geekpi.get(ddns_queue[0],"record_line")
)
# 获取来自config.txt的配置参数
current_ip = None

def ddns(ip):
    params.update(dict(value=ip))
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/json"}
    conn = httplib.HTTPSConnection("dnsapi.cn")
    conn.request("POST", "/Record.Ddns", urllib.urlencode(params), headers)
    
    response = conn.getresponse()
    print response.status, response.reason
    data = response.read()
    print data
    conn.close()
    return response.status == 200
#获取来自dnsapi.cn返回的json数据，dns记录中的ip地址
def getip():
    sock = socket.create_connection(('ns1.dnspod.net', 6666))
    ip = sock.recv(16)
    sock.close()
    return ip
#获取现在的外网ip地址
if __name__ == '__main__':
    while True:
        try:
            ip = getip()
            print ip
            if current_ip != ip:
                if ddns(ip):
                    current_ip = ip
        except Exception, e:
            print e
            pass
        time.sleep(30)
#如不同，则修改记录，每隔30秒检查一次