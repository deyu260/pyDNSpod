#!/usr/bin/env python
#-*- coding:utf-8 -*-

import httplib, urllib
import socket
import time

params = dict(
    login_email="Admin@dnspod.cn", # replace with your email�滻Ϊ�����˺ŵ�ַ
    login_password="password", # replace with your password�滻Ϊ����
    format="json",
    domain_id=100, # replace with your domain_od, can get it by API Domain.List�滻Ϊ����id
    record_id=100, # replace with your record_id, can get it by API Record.List
    sub_domain="mail", # replace with your sub_domain�滻Ϊ����������������@
    record_line="Ĭ��",
)
current_ip = None

def ddns(ip):
    params.update(dict(value=ip))
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/json"}
    conn = httplib.HTTPSConnection("dnsapi.cn")
    conn.request("POST", "/Record.Ddns", urllib.urlencode(params), headers)
    
    response = conn.getresponse()
    print response.status, response.reason
    data = response.read()
    print data
    conn.close()
    return response.status == 200

def getip():
    sock = socket.create_connection(('ns1.dnspod.net', 6666))
    ip = sock.recv(16)
    sock.close()
    return ip

if __name__ == '__main__':
    while True:
        try:
            ip = getip()
            print ip
            if current_ip != ip:
                if ddns(ip):
                    current_ip = ip
        except Exception, e:
            print e
            pass
        time.sleep(30)