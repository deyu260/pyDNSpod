﻿import httplib, urllib
import json
import ConfigParser
import sys

domains_id =[]
domains_name =[]
domains_records =[]
records_id =[]
records_name=[]
records_line=[]
records_type=[]
records_ttl=[]
records_value=[]
records_mx=[]
records_enabled=[]
records_monitor_status=[]
records_remark=[]
records_updated_on=[]

#初始化记录表

login_email = raw_input("input dnspod.cn login email : ")
login_password = raw_input("input dnspod.cn login password : ")
#用户按提示输入注册dnspod.cn的邮箱账户密码

params = {
	"login_email":login_email,
    "login_password":login_password,
    "format":"json"
}

headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/json"}
conn = httplib.HTTPSConnection("dnsapi.cn")
conn.request("POST", "/Domain.List", urllib.urlencode(params), headers)
response = conn.getresponse()
data = response.read()
receive2 = json.loads(data)
#将账号信息发至dnsapi.cn返回json数据

domain_total = receive2.get("info").get("domain_total")
for i in range(0 , domain_total):
	domains_id.append(receive2.get("domains")[i].get("id"))
	domains_name.append(receive2.get("domains")[i].get("name"))
	domains_records.append(receive2.get("domains")[i].get("records"))
	print i," : ",domains_name[i],"  have ",domains_records[i]," records \n"
#将返回的记录打印出来
	
record_queue_number = int(raw_input("which domain to set to ddns \n [0..n] in front of upon lines :"))
#用户按提示输入要修改的域名的记录号

params = {
	"login_email":login_email,
    "login_password":login_password,
    "format":"json",
	"domain_id":domains_id[record_queue_number]
}
conn.request("POST", "/Record.List", urllib.urlencode(params), headers)
response = conn.getresponse()
data = response.read()
receive = json.loads(data)
for j in range(0 , int(domains_records[record_queue_number])):
	records_id.append(receive.get("records")[j].get("id"))
	records_name.append(receive.get("records")[j].get("name"))
	records_line.append(receive.get("records")[j].get("line"))
	records_type.append(receive.get("records")[j].get("type"))
	records_ttl.append(receive.get("records")[j].get("ttl"))
	records_value.append(receive.get("records")[j].get("value"))
	records_mx.append(receive.get("records")[j].get("mx"))
	records_enabled.append(receive.get("records")[j].get("enabled"))
	records_monitor_status.append(receive.get("records")[j].get("monitor_status"))
	records_remark.append(receive.get("records")[j].get("remark"))
	records_updated_on.append(receive.get("records")[j].get("updated_on"))
	print j," : ",records_id[j]," ",records_name[j]," ",records_line[j]," ",records_type[j]," ",records_ttl[j]," ",records_value[j]," ",records_mx[j]," ",records_enabled[j]," ",records_monitor_status[j]," ",records_remark[j]," ",records_updated_on[j]
#打印该记录号查询所得的子域名记录号及其所有信息
	
recordnumber =  int(raw_input("which record will be set to dynamic IP address :"))
#用户按提示输入要修改的子域名的记录号

print recordnumber," : "
print " login_email = ",login_email
print " login_password = ",login_password
print " domain_id = ",domains_id[record_queue_number]
print " record_id = ",records_id[recordnumber]
print " sub_domain = ",records_name[recordnumber]
print " record_line = ",records_line[recordnumber]
print "will write to configure.txt file"

geekpi = ConfigParser.ConfigParser()
geekpi.add_section('ddns1')
geekpi.set('ddns1',"login_email",login_email)
geekpi.set('ddns1',"login_password",login_password)
geekpi.set('ddns1',"domain_id",domains_id[record_queue_number])
geekpi.set('ddns1',"record_id",records_id[recordnumber])
geekpi.set('ddns1',"sub_domain",records_name[recordnumber])
geekpi.set('ddns1',"record_line",records_line[recordnumber])

reload(sys)
sys.setdefaultencoding('utf-8') 
#设置文件为utf-8格式避免乱码

with open("configure.txt","wb") as configfile :
	geekpi.write(configfile)
#记录写入configure.txt

conn.close()
print "OK"
